const usersResolver = {
    Query: {
        userDetailById: (_,{userId},{dataSourses, userIdToken}) => {
            if(userId == userIdToken)
                return dataSourses.authAPI.getUser(userId)
            else
                return null
        },
    },
    Mutation: {
        crearUsuario: async(_,{user},{dataSourses}) => {
            const authInput = {
                username: user.username,
                password: user.password,
                name: user.name,
                email: user.email,
                address: user.address,
                number: user.number
            }
            return await dataSourses.authAPI.createUser(authInput);
        },
        logIn: (_,{credentials},{dataSourses}) => dataSourses.authAPI.authRequest(credentials),
        refreshToken: (_,{refresh},{dataSourses}) => dataSourses.authAPI.refreshToken(refresh),
    }
};

module.exports = usersResolver;