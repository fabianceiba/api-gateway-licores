const catalogResolver = {
    Query: {
        licorById: async(_,{idLicor},{dataSourses, userIdToken}) => {
            if(idLicor == userIdToken){
                return dataSourses.catalogAPI.licorById(idLicor)
            }else{
                return null
            }
        },
    },
    Mutation: {
        crearLicor: async(_,{licor},{dataSources}) => {
            const catalogInput = {
                id: licor.id,
                tipoLicor: licor.tipoLicor,
                nombreLicor: licor.nombreLicor,
                precio: licor.precio,
                presentacionLicor: licor.presentacionLicor,
            }
            return await dataSources.catalogAPI.crearLicor(catalogInput);
        }
    }
};

module.exports = catalogResolver;