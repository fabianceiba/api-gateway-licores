const authResolver = require('./auth_resolver');
const catalogResolver = require('./catolog_resolver');

const lodash = require('lodash');

const resolvers = lodash.merge(authResolver,catalogResolver);

module.exports = resolvers;