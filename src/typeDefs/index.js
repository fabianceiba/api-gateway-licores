//Se llama al typedef (esquema) de cada submodulo
const catalogTypeDefs = require('./catalog_type_defs');
const authTypeDefs = require('./auth_type_defs');
//Se unen
const schemasArrays = [authTypeDefs, catalogTypeDefs];
//Se exportan
module.exports = schemasArrays;