const { gql } = require('apollo-server');

const authTypeDefs = gql`
    type Tokens {
        refresh: String!
        access: String!
    }
    
    type Access {
        access: String!
    }
    
    input CredentialsInput {
        username: String!
        password: String!
    }
    input SignUpInput {
        id: Int!
        username: String!
        password: String!
        name: String!
        email: String!
        address: String!
        number: String!
    }
    type UserDetail {
        id: Int!
        username: String!
        password: String!
        name: String!
        email: String!
        address: String!
        number: String!
    }
    type Mutation {
        logIn(credentials: CredentialsInput!): Tokens!
        refreshToken(refresh: String!): Access!
        crearUsuario(user: SignUpInput!): UserDetail!
        
    }
    type Query {
        userDetailById(userId: Int!): UserDetail!
    }
`;
module.exports = authTypeDefs;

