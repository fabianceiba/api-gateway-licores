const { gql } = require('apollo-server');

const accountTypeDefs = gql`
type licoresType{
    id: Int!
    tipoLicor: String!
    nombreLicor: String!
    precio: Float!
    presentacionLicor: String!
}

input licoresInput{
    id: Int!
    tipoLicor: String!
    nombreLicor: String!
    precio: Float!
    presentacionLicor: String!
}
  
type Query{
    licorById(idLicor:Int!):licoresType!
}

type Mutation{
    crearLicor(licor:licoresInput!):licoresType!
}
`;

module.exports = accountTypeDefs;

