const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');
class CatalogAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = serverConfig.catalog_api_url;
    }

    async crearLicor(licor) {
        licor = new Object(JSON.parse(JSON.stringify(licor)));
        return await this.post('/licores', licor);
    }

    async licorById(id) {
        return await this.get(`/licores/${id}`);
    }

}
module.exports = CatalogAPI;